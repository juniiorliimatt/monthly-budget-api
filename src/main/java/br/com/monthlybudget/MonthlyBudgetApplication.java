package br.com.monthly.budget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonthlyBudgetApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonthlyBudgetApplication.class, args);
	}

}
