package br.com.monthlybudget.models;


public enum SpentType {

    ESSENTIAL,
    FOLKS,
    ECONOMY

}
