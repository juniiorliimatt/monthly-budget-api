package br.com.monthlybudget.repositories;

import br.com.monthlybudget.domains.MyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface MyUserRepository extends JpaRepository<MyUser, UUID> {

  Optional<MyUser> findByEmail(String email);

}
