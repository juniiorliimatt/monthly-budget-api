package br.com.monthlybudget.resources;

import br.com.monthlybudget.security.JWTUtil;
import br.com.monthlybudget.domains.MyUser;
import br.com.monthlybudget.models.LoginCredentials;
import br.com.monthlybudget.repositories.MyUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

  //TODO Refatorar, usar camada de serviço.
  private final MyUserRepository userRepository;
  private final JWTUtil jwtUtil;
  private final AuthenticationManager authenticationManager;
  private final PasswordEncoder passwordEncoder;

  @PostMapping("/register")
  public Map<String, Object> registerHandler(@RequestBody MyUser myUser) {
	String encodedPass = passwordEncoder.encode(myUser.getPassword());
	myUser.setPassword(encodedPass);
	myUser = userRepository.save(myUser);
	String token = jwtUtil.generateToken(myUser.getEmail());
	return Collections.singletonMap("jwt-token", token);
  }

  @PostMapping("/login")
  public Map<String, Object> loginHandler(@RequestBody LoginCredentials loginCredentials) {
	//TODO melhorar com exceptionHandler
	try {
	  var authInputToken = new UsernamePasswordAuthenticationToken(loginCredentials.getEmail(),
																   loginCredentials.getPassword());

	  authenticationManager.authenticate(authInputToken);

	  String token = jwtUtil.generateToken(loginCredentials.getEmail());

	  return Collections.singletonMap("jwt-token", token);
	} catch(AuthenticationException exception) {
	  throw new RuntimeException("Invalid Login Credentials");
	}
  }

}
