package br.com.monthlybudget.resources;

import br.com.monthlybudget.services.UserService;
import br.com.monthlybudget.domains.MyUser;
import br.com.monthlybudget.repositories.MyUserRepository;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class UserController {

  private final UserService userService;
  private final MyUserRepository userRepository;

  @GetMapping
  public ResponseEntity<List<MyUser>> getAllUsers() {
	return ResponseEntity.ok(userService.findAll());
  }

  @GetMapping("/{id}")
  public ResponseEntity<MyUser> getUser(@PathVariable final UUID id) {
	return ResponseEntity.ok(userService.get(id));
  }

  @PostMapping
  @ApiResponse(responseCode = "201")
  public ResponseEntity<UUID> createUser(@RequestBody @Valid final MyUser myUser) {
	//TODO refatorar usar URI no retorno
	return new ResponseEntity<>(userService.create(myUser), HttpStatus.CREATED);
  }

  @PutMapping("/{id}")
  public ResponseEntity<Void> updateUser(@PathVariable final UUID id, @RequestBody @Valid final MyUser myUser) {
	userService.update(id, myUser);
	return ResponseEntity.ok()
						 .build();
  }

  @DeleteMapping("/{id}")
  @ApiResponse(responseCode = "204")
  public ResponseEntity<Void> deleteUser(@PathVariable final UUID id) {
	userService.delete(id);
	return ResponseEntity.noContent()
						 .build();
  }

  @GetMapping("/info")
  public MyUser getUserDetails() {
	//TODO refatorar
	String email = (String) SecurityContextHolder.getContext()
												 .getAuthentication()
												 .getPrincipal();
	return userRepository.findByEmail(email)
						 .get();
  }

}
