package br.com.monthlybudget.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author Junior lima - juniiorliimatt@gmail.com
 * @since 12/05/2022
 */

@Component
public class JWTUtil {

  @Value("${jwt_secret}")
  private String secret;

  public String generateToken(String email) throws IllegalArgumentException, JWTCreationException {
	return JWT.create()
			  .withSubject("USer Details")
			  .withClaim("email", email)
			  .withIssuedAt(new Date())
			  .withIssuer("monthly_budget")
			  .sign(Algorithm.HMAC256(secret));
  }

  public String validateTokenAndRetrieveSubject(String token) throws JWTVerificationException {
	JWTVerifier verifier = JWT.require(Algorithm.HMAC256(secret))
							  .withSubject("User Details")
							  .withIssuer("monthly_budget")
							  .build();
	DecodedJWT jwt = verifier.verify(token);

	return jwt.getClaim("email")
			  .asString();
  }

}
