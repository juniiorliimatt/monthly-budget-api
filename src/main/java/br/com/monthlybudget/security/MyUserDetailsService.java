package br.com.monthlybudget.security;

import br.com.monthlybudget.repositories.MyUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class MyUserDetailsService implements UserDetailsService {

  private final MyUserRepository userRepository;

  @Autowired
  public MyUserDetailsService(MyUserRepository userRepository) {
	this.userRepository = userRepository;
  }

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
	var userRes = userRepository.findByEmail(email);
	if(userRes.isEmpty()) {
	  throw new UsernameNotFoundException("Could not findUser with email = " + email);
	}
	var user = userRes.get();
	return new User(email, user.getPassword(), Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
  }

}
