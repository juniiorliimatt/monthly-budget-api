package br.com.monthlybudget.services;

import br.com.monthlybudget.domains.MyUser;
import br.com.monthlybudget.repositories.MyUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.UUID;


@Service
@RequiredArgsConstructor
public class UserService {

  private final MyUserRepository userRepository;

  public List<MyUser> findAll() {
	return userRepository.findAll();
  }

  public MyUser get(final UUID id) {
	return userRepository.findById(id)
						 .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
  }

  public UUID create(final MyUser myUser) {
	return userRepository.save(myUser)
						 .getId();
  }

  public void update(final UUID id, final MyUser myUser) {
	userRepository.findById(id)
				  .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	userRepository.save(myUser);
  }

  public void delete(final UUID id) {
	userRepository.deleteById(id);
  }

}
