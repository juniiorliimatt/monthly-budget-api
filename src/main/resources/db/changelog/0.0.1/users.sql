--liquibase formatted sql

-- changeset author:junior.lima
-- comment create table users
-- preconditions onFail:MARK_RAN onError:HALT
CREATE TABLE IF NOT EXISTS ${schema}.users
(
    id         uuid NOT NULL primary key,
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    first_name VARCHAR(100),
    last_name  VARCHAR(100),
    email      VARCHAR(100),
    password   VARCHAR(100)
)
